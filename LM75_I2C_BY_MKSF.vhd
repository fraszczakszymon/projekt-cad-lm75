-- Pakiet do obsługi termometru LM75 --
-- Autorzy: --
-- Marcin Krawczyk --
-- Szymon Frąszczak --
LIBRARY IEEE;   
USE IEEE.std_logic_1164.ALL;
LIBRARY FMF;   
USE FMF.conversions.ALL;

PACKAGE LM75_I2C_BY_MKSF IS
  procedure setSlaveID(signal a0, a1, a2: out std_logic;
                       constant a, b, c: in std_logic);
  
  procedure sendSlaveID(signal s: out std_logic; 
                        signal clk: in std_logic; 
                        constant a, b, c: in std_logic;
                        constant qperiod,delay: in delay_length);
  
  procedure sendTemperaturePointer(signal s: out std_logic;
                                   constant qperiod: in delay_length);
  
  procedure sendConfigurationPointer(signal s: out std_logic;
                                     constant qperiod, delay: in delay_length);
  
  procedure sendTosPointer(signal s: out std_logic;
                           constant qperiod, delay: in delay_length);
  
  procedure sendThystPointer(signal s: out std_logic;
                             constant qperiod,delay: in delay_length);
  
  procedure sendConfigurationRegister(signal s: out std_logic;
                                      constant qperiod,delay: in delay_length;
                                      constant fault_queue, os_polarity, cmp_int, shutdown: in integer);
  
  procedure sendFaultQueue(signal s: out std_logic; 
                           constant f_q: in integer;
                           constant qperiod,delay: in delay_length);
  
  procedure sendOSPolarity(signal s: out std_logic; 
                           constant os_pol: in integer;
                           constant qperiod,delay: in delay_length);
  
  procedure sendCmpInt(signal s: out std_logic; 
                       constant c_i: in integer;
                       constant qperiod,delay: in delay_length);
  
  procedure sendShutdown(signal s: out std_logic; 
                         constant shtdn: in integer;
                         constant qperiod, delay: in delay_length);
  
  procedure sendReadBit(signal s: out std_logic;
                        constant qperiod,delay: in delay_length);
  
  procedure sendWriteBit(signal s: out std_logic;
                         constant qperiod: in delay_length);
  
  procedure sendStopBit(signal s: out std_logic;
                        constant qperiod: in delay_length);
                        
  procedure readByte(constant qperiod: in delay_length);
                        
  procedure masterNotAcknowledge(signal s: out std_logic;
                        constant qperiod: in delay_length);
                        
  procedure waitForDeviceAcknowledge(constant qperiod: in delay_length);
  
  procedure sendMasterAcknowledge(signal s: out std_logic;
                                  constant qperiod,delay: in delay_length);
  
  procedure sendData(signal s: out std_logic; 
                     constant temp: in integer;
                     constant qperiod,delay: in delay_length);
  
  procedure readDataFromTemperatureRegister(signal s: out std_logic; 
                                            signal clk: in std_logic; 
                                            constant a, b, c: in std_logic;
                                            constant qperiod,delay: in delay_length);
  
  procedure readDataFromTosRegister(signal s: out std_logic; 
                                    signal clk: in std_logic; 
                                    constant a, b, c: in std_logic;
                                    constant qperiod,delay: in delay_length);
  
  procedure writeDataToTosRegister(signal s: out std_logic; 
                                   signal clk: in std_logic; 
                                   constant a, b, c: in std_logic; 
                                   constant temp: in integer;
                                   constant qperiod,delay: in delay_length);
  
  procedure writeDataToConfigRegister(signal s: out std_logic; 
                                      signal clk: in std_logic; 
                                      constant a, b, c: in std_logic; 
                                      constant fault_queue, os_polarity, cmp_int, shutdown: in integer;
                                      constant qperiod,delay: in delay_length);
  
  procedure readDataFromConfigRegister(signal s: out std_logic; 
                                       signal clk: in std_logic; 
                                       constant a, b, c: in std_logic;
                                       constant qperiod,delay: in delay_length);
  
  procedure readDataFromThystRegister(signal s: out std_logic; 
                                      signal clk: in std_logic; 
                                      constant a, b, c: in std_logic;
                                      constant qperiod,delay: in delay_length);
  
  procedure writeDataToThystRegister(signal s: out std_logic; 
                                     signal clk: in std_logic; 
                                     constant a, b, c: in std_logic; 
                                     constant temp: in integer;
                                     constant qperiod,delay: in delay_length);
  
  procedure clk(signal s: out std_logic;
                constant qperiod: in delay_length); 
	
END LM75_I2C_BY_MKSF;


PACKAGE BODY LM75_I2C_BY_MKSF IS
  procedure setSlaveID(signal a0, a1, a2: out std_logic; constant a, b, c: in std_logic) is
  begin
    a0 <= a;
    a1 <= b;
    a2 <= c;
  end procedure;
  
  procedure sendSlaveID(signal s: out std_logic; signal clk: in std_logic; constant a, b, c: in std_logic; constant qperiod,delay: in delay_length) is
  begin
    if rising_edge(clk) then
      s <= '1', '0' after qperiod;
      wait for 4*qperiod - delay;
      for i in 1 to 4 loop
        if i = 1 or i = 4 then 
          s <= '1', '0' after 2*qperiod + delay;
          wait for 4*qperiod;
        else
          s <= '0';
          wait for 4*qperiod;
        end if;
      end loop;
      if a = '1' then
        s <= '1', '0' after 2*qperiod + delay;
        wait for 4*qperiod;
      else
        s <= '0';
        wait for 4*qperiod;
      end if;
      if b = '1' then
        s <= '1', '0' after 2*qperiod + delay;
        wait for 4*qperiod;
      	else
        s <= '0';
        wait for 4*qperiod;
      end if;
      if c = '1' then
        s <= '1', '0' after 2*qperiod + delay;
        wait for 4*qperiod;
      else
        s <= '0';
        wait for 4*qperiod;
      end if;
  --    s <= 'Z';
    else
      wait for delay;
      sendSlaveID(s, clk, a, b, c, qperiod, delay);
  --    s <= 'Z';
    end if;   
  end procedure;
  
  procedure sendTemperaturePointer(signal s: out std_logic; constant qperiod: in delay_length) is
  begin
    for i in 1 to 8 loop
      s <= '0';
      wait for 4*qperiod;
    end loop;
    s <= 'Z';
  end procedure;
  
  procedure sendConfigurationPointer(signal s: out std_logic; constant qperiod,delay: in delay_length) is
  begin
    for i in 1 to 7 loop
      s <= '0';
      wait for 4*qperiod;
    end loop;
    s <= '1';
    wait for 2*qperiod + delay;
    s <= 'Z';
  end procedure;
  
  procedure sendTosPointer(signal s: out std_logic; constant qperiod,delay: in delay_length) is
  begin
    for i in 1 to 6 loop
      s <= '0';
      wait for 4*qperiod;
    end loop;
    for j in 1 to 2 loop
      s <= '1', '0' after 2*qperiod + delay;
      wait for 4*qperiod;
    end loop;
    s <= 'Z';
  end procedure;
  
  procedure sendThystPointer(signal s: out std_logic; constant qperiod,delay: in delay_length) is
  begin
    for i in 1 to 6 loop
      s <= '0';
      wait for 4*qperiod;
    end loop;
    s <= '1', '0' after 2*qperiod + delay;
    wait for 2*4*qperiod;
    s <= 'Z';
  end procedure;
  
  procedure sendConfigurationRegister(signal s: out std_logic; constant qperiod,delay: in delay_length; constant fault_queue, os_polarity, cmp_int, shutdown: in integer) is
  begin
    for i in 1 to 3 loop
      s <= '0';
      wait for 4*qperiod;
    end loop;
    
    sendFaultQueue(s, fault_queue, qperiod, delay);
    sendOSPolarity(s, os_polarity, qperiod, delay);
    sendCmpInt(s, cmp_int, qperiod, delay);
    sendShutdown(s, shutdown, qperiod, delay);
    --s <= 'Z';
  end procedure;
  
  procedure sendFaultQueue(signal s: out std_logic; constant f_q: in integer; constant qperiod,delay: in delay_length) is
  begin
    wait for qperiod;
    if f_q = 1 then
      wait for qperiod - delay;
      for i in 1 to 2 loop
        s <= '0';
        wait for 4*qperiod;
      end loop;
      s <= 'Z';
    elsif f_q = 2 then
      wait for qperiod - delay;
      s <= '0';
      wait for 4*qperiod;
      s <= '1', '0' after 2*qperiod + delay;
      wait for 4*qperiod; 
      s <= 'Z';
    elsif f_q = 4 then
      wait for qperiod - delay;
      s <= '1', '0' after 2*qperiod + delay;
      wait for 4*qperiod;
      s <= '0';
      wait for 4*qperiod; 
      s <= 'Z';
    elsif f_q = 6 then
      wait for qperiod - delay;
      for i in 1 to 2 loop
        s <= '1', '0' after 2*qperiod + delay;
        wait for 4*qperiod;
      end loop;
      s <= 'Z';
    end if;
  end procedure;
  
  procedure sendOSPolarity(signal s: out std_logic; constant os_pol: in integer; constant qperiod,delay: in delay_length) is
  begin
    if os_pol = 0 then
      s <= '0';
      wait for 4*qperiod;
    elsif os_pol = 1 then
      s <= '1', '0' after 2*qperiod + delay;
      wait for 4*qperiod;
    end if;
    s <= 'Z';
  end procedure;
  
  procedure sendCmpInt(signal s: out std_logic; constant c_i: in integer; constant qperiod,delay: in delay_length) is
  begin
    if c_i = 0 then
      s <= '0';
      wait for 4*qperiod;
    elsif c_i = 1 then
      s <= '1', '0' after 2*qperiod + delay;
      wait for 4*qperiod;
    end if;
    s <= 'Z';
  end procedure;
  
  procedure sendShutdown(signal s: out std_logic; constant shtdn: in integer; constant qperiod,delay: in delay_length) is
  begin
    if shtdn = 0 then
      s <= '0';
      wait for 4*qperiod;
    elsif shtdn = 1 then
      s <= '1', '0' after 2*qperiod + delay;
      wait for 4*qperiod;
    end if;
    s <= 'Z';
  end procedure;
  
  procedure sendReadBit(signal s: out std_logic; constant qperiod,delay: in delay_length) is
  begin
    s <= '1', 'Z' after 2*qperiod + delay;
    wait for 4*qperiod;
    --s <= 'Z';
  end procedure;
  
  procedure sendWriteBit(signal s: out std_logic; constant qperiod: in delay_length) is
  begin
    s <= '0', 'Z' after 2*qperiod;
    wait for 4*qperiod;
  end procedure;
  
  procedure readByte(constant qperiod: in delay_length) is
  begin
    wait for 8*4*qperiod;
  end procedure;
  
  procedure sendStopBit(signal s: out std_logic; constant qperiod: in delay_length) is 
  begin
    s <= '0', '1' after qperiod;
    wait for 2*qperiod;
    --s <= '1';
  end procedure;
  
  procedure masterNotAcknowledge(signal s: out std_logic; constant qperiod: in delay_length) is 
  begin
    s <= '1', '0' after 2*qperiod;
    wait for 4*qperiod;
  end procedure;
  
  procedure waitForDeviceAcknowledge(constant qperiod: in delay_length) is
  begin
    wait for 4*qperiod;
  end procedure;
  
  procedure sendMasterAcknowledge(signal s: out std_logic; constant qperiod,delay: in delay_length) is
  begin
    s <= '0', 'Z' after 2*qperiod;
    wait for 4*qperiod;
    --s <= 'Z';
    
  end procedure;
  
  procedure sendData(signal s: out std_logic; constant temp: in integer; constant qperiod,delay: in delay_length) is
  variable v_temp: std_logic_vector(31 downto 0);
  variable tmp: std_logic_vector(7 downto 0);
  begin
    v_temp := int_to_slv(temp);
    tmp := v_temp(7 downto 0);
    for i in 0 to 7 loop
      if tmp(7 - i) = '1' then
        s <= '1', '0' after 2*qperiod + delay;
        wait for 4*qperiod;
      else
        s <= '0';
        wait for 4*qperiod;
      end if;
    end loop;
    s <= 'Z';
    wait for 4*qperiod;
    for j in 0 to 7 loop
      s <= '0';
      wait for 4*qperiod;
    end loop;
    s <= 'Z';
  end procedure;
  
  procedure readDataFromTemperatureRegister(signal s: out std_logic; signal clk: in std_logic; constant a, b, c: in std_logic; constant qperiod,delay: in delay_length) is
  begin
    if rising_edge(clk) then
      sendSlaveID(s, clk, a, b, c, qperiod, delay);
      sendWriteBit(s, qperiod);
      waitForDeviceAcknowledge(qperiod);
      sendTemperaturePointer(s, qperiod);
      waitForDeviceAcknowledge(qperiod);
      sendSlaveID(s, clk, a, b, c, qperiod, delay);
     	sendReadBit(s, qperiod, delay);
     	readByte(qperiod);
      waitForDeviceAcknowledge(qperiod);
      sendMasterAcknowledge(s, qperiod, delay);
     	readByte(qperiod);
     	masterNotAcknowledge(s, qperiod);
     	sendStopBit(s, qperiod);
    else
      WAIT FOR delay;
      readDataFromTemperatureRegister(s, clk, a, b, c, qperiod, delay);
    end if;
  end procedure;
  
  procedure readDataFromTosRegister(signal s: out std_logic; signal clk: in std_logic; constant a, b, c: in std_logic; constant qperiod,delay: in delay_length) is
  begin
    if rising_edge(clk) then
      sendSlaveID(s, clk, a, b, c, qperiod, delay);
      sendWriteBit(s, qperiod);
      waitForDeviceAcknowledge(qperiod);
      sendTosPointer(s, qperiod, delay);
      waitForDeviceAcknowledge(qperiod);
      sendSlaveID(s, clk, a, b, c, qperiod, delay);
     	sendReadBit(s, qperiod, delay);
     	readByte(qperiod);
      waitForDeviceAcknowledge(qperiod);
      sendMasterAcknowledge(s, qperiod, delay);
     	readByte(qperiod);
     	masterNotAcknowledge(s, qperiod);
     	sendStopBit(s, qperiod);
    else
      WAIT FOR delay;
      readDataFromTosRegister(s, clk, a, b, c,qperiod, delay);
    end if;
  end procedure;
  
  procedure writeDataToTosRegister(signal s: out std_logic; signal clk: in std_logic; constant a, b, c: in std_logic; constant temp: in integer; constant qperiod,delay: in delay_length) is
  begin
    if rising_edge(clk) then
      sendSlaveID(s, clk, a, b, c, qperiod, delay);
      sendWriteBit(s, qperiod);
      waitForDeviceAcknowledge(qperiod);
      sendTosPointer(s, qperiod, delay);
      waitForDeviceAcknowledge(qperiod);
      sendData(s, temp, qperiod, delay);
      waitForDeviceAcknowledge(qperiod);
      sendStopBit(s, qperiod);
    else
      WAIT FOR delay;
      writeDataToTosRegister(s, clk, a, b, c, temp, qperiod, delay);
    end if;
  end procedure;
  
  procedure writeDataToConfigRegister(signal s: out std_logic; signal clk: in std_logic; constant a, b, c: in std_logic; constant fault_queue, os_polarity, cmp_int, shutdown: in integer; constant qperiod,delay: in delay_length) is
  begin
    if rising_edge(clk) then
      sendSlaveID(s, clk, a, b, c, qperiod, delay);
      sendWriteBit(s, qperiod);
      waitForDeviceAcknowledge(qperiod);
      sendConfigurationPointer(s, qperiod, delay);
      waitForDeviceAcknowledge(qperiod);
      sendConfigurationRegister(s, qperiod, delay, fault_queue, os_polarity, cmp_int, shutdown);
      waitForDeviceAcknowledge(qperiod);
      sendStopBit(s, qperiod);
    else
      wait for delay;
      writeDataToConfigRegister(s, clk, a, b, c, fault_queue, os_polarity, cmp_int, shutdown, qperiod, delay);
    end if;
  end procedure;
  
  procedure readDataFromConfigRegister(signal s: out std_logic; signal clk: in std_logic; constant a, b, c: in std_logic; constant qperiod,delay: in delay_length) is
  begin
    if rising_edge(clk) then
      sendSlaveID(s, clk, a, b, c, qperiod, delay);
      sendWriteBit(s, qperiod);
      waitForDeviceAcknowledge(qperiod);
      sendConfigurationPointer(s, qperiod, delay);
      waitForDeviceAcknowledge(qperiod);
      sendSlaveID(s, clk, a, b, c, qperiod, delay);
     	sendReadBit(s, qperiod, delay);
     	waitForDeviceAcknowledge(qperiod);
     	readByte(qperiod);
     	masterNotAcknowledge(s, qperiod);
     	sendStopBit(s, qperiod);
    else
      wait for delay;
      readDataFromConfigRegister(s, clk, a, b, c, qperiod, delay);
    end if;
  end procedure;
  
  procedure readDataFromThystRegister(signal s: out std_logic; signal clk: in std_logic; constant a, b, c: in std_logic; constant qperiod,delay: in delay_length) is
  begin 
    if rising_edge(clk) then
      sendSlaveID(s, clk, a, b, c, qperiod, delay);
      sendWriteBit(s, qperiod);
      waitForDeviceAcknowledge(qperiod);
      sendThystPointer(s, qperiod, delay);
      waitForDeviceAcknowledge(qperiod);
      sendSlaveID(s, clk, a, b, c, qperiod, delay);
     	sendReadBit(s, qperiod, delay);
     	readByte(qperiod);
      waitForDeviceAcknowledge(qperiod);
      sendMasterAcknowledge(s, qperiod, delay);
     	readByte(qperiod);
     	masterNotAcknowledge(s, qperiod);
     	sendStopBit(s, qperiod);
    else
      wait for delay;
      readDataFromThystRegister(s, clk, a, b, c, qperiod, delay);
    end if;
  end procedure;
  
  procedure writeDataToThystRegister(signal s: out std_logic; signal clk: in std_logic; constant a, b, c: in std_logic; constant temp: in integer; constant qperiod,delay: in delay_length) is
  begin
    if rising_edge(clk) then
      sendSlaveID(s, clk, a, b, c, qperiod, delay);
      sendWriteBit(s, qperiod);
      waitForDeviceAcknowledge(qperiod);
      sendThystPointer(s, qperiod, delay);
      waitForDeviceAcknowledge(qperiod);
      sendData(s, temp, qperiod, delay);
      waitForDeviceAcknowledge(qperiod);
      sendStopBit(s, qperiod);
    else
      WAIT FOR delay;
      writeDataToThystRegister(s, clk, a, b, c, temp, qperiod, delay);
    end if;
  end procedure;
  
  procedure clk(signal s: out std_logic; constant qperiod: in delay_length) is
	begin
	  loop
	    s <= '0', '1' after 2*qperiod;
	    wait for 4*qperiod;
	  end loop;
	  
	end procedure;
  
END LM75_I2C_BY_MKSF;