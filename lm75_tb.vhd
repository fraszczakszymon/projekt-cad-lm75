-- Pakiet do obs?ugi termometru LM75 --
-- Autorzy: --
-- Marcin Krawczyk --
-- Szymon Fr?szczak --
LIBRARY  IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE STD.TEXTIO.ALL;
LIBRARY FMF;   
USE FMF.conversions.ALL;
LIBRARY LM75_I2C_BY_MKSF;
USE LM75_I2C_BY_MKSF.LM75_I2C_BY_MKSF.ALL;

ENTITY lm75_tb IS	 
  	
END lm75_tb;		

ARCHITECTURE testbench_arch OF lm75_tb IS


-- deklaracja komponentu ----------------------------------------
	COMPONENT lm75
    PORT (SCL : IN std_logic;
          SDA : INOUT std_logic;
          A0 : IN std_logic;
          A1 : IN std_logic;
          A2 : IN std_logic;
          OS : OUT std_logic
          );
		
	END COMPONENT;
	
	SIGNAL SCL : std_logic;
	SIGNAL SDA : std_logic;
	SIGNAL SDA_in : std_logic := '1';
	SIGNAL SDA_out : std_logic;
	SIGNAL A0 : std_logic := 'U';
	SIGNAL A1 : std_logic := 'U';
	SIGNAL A2 : std_logic := 'U';
	SIGNAL OS : std_logic := 'U';
	constant delay : delay_length := 25 ns;
	constant qperiod: delay_length := 625 ns; -- 1/4 of clk period
	constant hperiod: delay_length := 2 * qperiod;
  constant period: delay_length := 2 * hperiod; --clk period
  
	
	BEGIN
	UUT : entity work.lm75					-- unit under test
	GENERIC MAP (
	             tsetup_SDA_SCL => 100 ns,           
               tsetup_SCL_SDAS => 600 ns,          
               tsetup_SCL_SDAP => 600 ns,  
	             thold_SDA_SCL => 900 ns,
               thold_SCL_SDAS => 600 ns,
	             tpw_SCL_posedge => 600 ns,             
               tpw_SCL_negedge => 1300 ns,
	             tperiod_SCL => 2500 ns,
	             temp_file_name => "temperatura.txt"
	  )
	PORT MAP (					-- mapowanie portow
	         SCL => SCL,
	         SDA => SDA,
	         A0 => A0,
	         A1 => A1,
	         A2 => A2,
	         OS => OS
		       );
	
	sim_end_process: process
  begin
    wait for 1 sec;
    wait for 150 * period;
    assert false
      report "End of simulation at time " & time'image(now)
      severity Failure;
  end process sim_end_process;
  
  SDA <= SDA_in;
  SDA_out <= SDA;
  clk(SCL, qperiod);  
  
  
  PROCESS
  BEGIN
  --wait for 0.15 sec;
  setSlaveID(A0, A1, A2, '1', '1', '1');  
  WAIT FOR 5*period;
  --readDataFromTemperatureRegister(SDA_in, SCL, '1', '1', '1', qperiod, delay);
  readDataFromConfigRegister(SDA_in, SCL, '1', '1', '1', qperiod, delay);
  wait for 5*period;
  writeDataToConfigRegister(SDA_in, SCL, '1', '1', '1', 2, 1, 0, 0, qperiod, delay);
  wait for 5*period;
  readDataFromConfigRegister(SDA_in, SCL, '1', '1', '1', qperiod, delay);
  wait for 5*period;
  readDataFromTosRegister(SDA_in, SCL, '1', '1', '1', qperiod, delay);
  wait for 5*period;
  writeDataToTosRegister(SDA_in, SCL, '1', '1', '1', 50, qperiod, delay);
  wait for 5*period;
  readDataFromTosRegister(SDA_in, SCL, '1', '1', '1', qperiod, delay);
  wait for 5*period;
  readDataFromThystRegister(SDA_in, SCL, '1', '1', '1', qperiod, delay);
  wait for 5*period;
  writeDataToThystRegister(SDA_in, SCL, '1', '1', '1', 45, qperiod, delay);
  wait for 5*period;
  readDataFromThystRegister(SDA_in, SCL, '1', '1', '1', qperiod, delay);
  wait for 0.15 sec;
  readDataFromTemperatureRegister(SDA_in, SCL, '1', '1', '1', qperiod, delay);
  wait for 0.15 sec;
  readDataFromTemperatureRegister(SDA_in, SCL, '1', '1', '1', qperiod, delay);
  wait for 0.15 sec;
  readDataFromTemperatureRegister(SDA_in, SCL, '1', '1', '1', qperiod, delay);
  wait for 0.15 sec;
  readDataFromTemperatureRegister(SDA_in, SCL, '1', '1', '1', qperiod, delay);
  wait for 0.15 sec;
  readDataFromTemperatureRegister(SDA_in, SCL, '1', '1', '1', qperiod, delay);
  
  WAIT;
    
  END PROCESS;
  
  

END testbench_arch;      

